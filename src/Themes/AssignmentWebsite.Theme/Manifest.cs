using OrchardCore.DisplayManagement.Manifest;

[assembly: Theme(
    Name = "AssignmentWebsite.Theme",
    Author = "sync2412",
    Website = "https://nik.uni-obuda.hu/",
    Version = "0.0.2",
    Description = "AssignmentWebsite.Theme"
)]

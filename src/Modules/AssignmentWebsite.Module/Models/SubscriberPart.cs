﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrchardCore.ContentManagement;

namespace AssignmentWebsite.Module.Models
{
    public class SubscriberPart : ContentPart
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}

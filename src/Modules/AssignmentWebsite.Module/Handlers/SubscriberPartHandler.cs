﻿using System.Threading.Tasks;
using AssignmentWebsite.Module.Models;
using OrchardCore.ContentManagement.Handlers;

namespace AssignmentWebsite.Module.Handlers
{
    public class SubscriberPartHandler : ContentPartHandler<SubscriberPart>
    {
        public override Task UpdatedAsync(UpdateContentContext context, SubscriberPart instance)
        {
            context.ContentItem.DisplayText = "Subscriber: " + instance.Email;
            return Task.CompletedTask;
        }
    }
}

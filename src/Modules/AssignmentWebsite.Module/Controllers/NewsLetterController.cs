﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AssignmentWebsite.Module.Models;
using GraphQL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.DependencyInjection;
using OrchardCore.ContentManagement;
using OrchardCore.ContentManagement.Records;
using YesSql;
using YesSql.Services;

namespace AssignmentWebsite.Module.Controllers
{
    public class NewsLetterController : Controller
    {
        private readonly ISession _session;
        private readonly IContentManager _contentManager;

        public NewsLetterController(ISession session, IContentManager contentManager)
        {
            _session = session;
            _contentManager = contentManager;
        }

        [HttpPost]
        [Route("AddSubscriber")]
        [IgnoreAntiforgeryToken]
        public async Task<JsonResult> AddSubscriber([FromBody] SubscriberPart objPart)
        {
            var subscribers = await _session
                .Query<ContentItem, ContentItemIndex>(index => index.ContentType == "SubscriberPage")
                .ListAsync();

            var count = subscribers.FirstOrDefault(item => item.As<SubscriberPart>().Email == objPart.Email);

            if (count != null)
            {
                return new JsonResult(new { Result = 0, Message = "E-mail already being used!" });
            }

            var contentItem = await _contentManager.NewAsync("SubscriberPage");

            var subscriberItem = contentItem.As<SubscriberPart>();
            subscriberItem.Name = objPart.Name;
            subscriberItem.Email = objPart.Email;
            subscriberItem.Apply();

            contentItem.DisplayText = "Subscriber: " + objPart.Email;

            await _contentManager.CreateAsync(contentItem, VersionOptions.Published);

            return new JsonResult(new { Result = 1, Message = "You have successfully subscribed!" });
        }

        [HttpGet]
        [Route("RemoveSubscriber")]
        [IgnoreAntiforgeryToken]
        public async Task<string> RemoveSubscriber(string email)
        {
            var subscribers = await _session
                .Query<ContentItem, ContentItemIndex>(index => index.ContentType == "SubscriberPage")
                .ListAsync();

            var toRemove = subscribers.FirstOrDefault(item => item.As<SubscriberPart>().Email == email);

            if (toRemove == null)
            {
                return "Error! Cannot remove the nothing!";
            }

            _session.Delete(toRemove);

            return "You have successfully unsubscribed!";
        }
    }
}

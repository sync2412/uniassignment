using OrchardCore.Modules.Manifest;

[assembly: Module(
    Name = "AssignmentWebsite.Module",
    Author = "sync2412",
    Website = "https://nik.uni-obuda.hu/",
    Version = "0.0.2",
    Description = "AssignmentWebsite.Module",
    Category = "Content Management"
)]

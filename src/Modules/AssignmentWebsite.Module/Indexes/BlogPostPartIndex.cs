﻿using System;
using OrchardCore.ContentManagement;
using YesSql.Indexes;

namespace AssignmentWebsite.Module.Indexes
{
    public class BlogPostPartIndex : MapIndex
    {
        public string ContentItemId { get; set; }
        public DateTime? PublishDate { get; set; }
    }

    public class BlogPostPartIndexProvider : IndexProvider<ContentItem>
    {
        public override void Describe(DescribeContext<ContentItem> context) =>
            context.For<BlogPostPartIndex>().Map(contentItem =>
            {
                if (contentItem.ContentType == "BlogPost")
                {
                    return new BlogPostPartIndex
                    {
                        ContentItemId = contentItem.ContentItemId,
                        PublishDate = contentItem.PublishedUtc
                    };
                }
                else
                {
                    return null;
                }
            });
    }
}

﻿using OrchardCore.ContentManagement;
using AssignmentWebsite.Module.Models;
using YesSql.Indexes;

namespace AssignmentWebsite.Module.Indexes
{
    public class SubscriberPartIndex : MapIndex
    {
        public string ContentItemId { get; set; }
    }

    public class SubscriberPartIndexProvider : IndexProvider<ContentItem>
    {
        public override void Describe(DescribeContext<ContentItem> context) =>
            context.For<SubscriberPartIndex>().Map(contentItem =>
            {
                var subscriberPart = contentItem.As<SubscriberPart>();

                return subscriberPart == null ? null : new SubscriberPartIndex
                {
                    ContentItemId = contentItem.ContentItemId
                };
            });
    }
}

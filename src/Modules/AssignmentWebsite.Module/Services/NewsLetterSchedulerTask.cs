﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AssignmentWebsite.Module.Models;
using OrchardCore.BackgroundTasks;
using OrchardCore.ContentManagement;
using OrchardCore.ContentManagement.Records;
using OrchardCore.Modules;
using YesSql;
using System.Net.Mail;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace AssignmentWebsite.Module.Services
{
    // Every week (“At 00:00 on Sunday.”): "0 0 * * 0"
    // Every minute: "*/1 * * * *"
    [BackgroundTask(Schedule = "*/1 * * * *", Description = "Sends a newsletter to every subscriber.")]
    public class NewsLetterSchedulerTask : IBackgroundTask
    {
        private readonly IServiceScopeFactory _scopeFactory;

        public NewsLetterSchedulerTask(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public async Task DoWorkAsync(IServiceProvider serviceProvider, CancellationToken cancellationToken)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var _session = scope.ServiceProvider.GetRequiredService<ISession>();
                var _clock = scope.ServiceProvider.GetRequiredService<IClock>();

                var subscribers = await _session
                    .Query<ContentItem, ContentItemIndex>(index => index.ContentType == "SubscriberPage")
                    .ListAsync();

                var weekThreshold = _clock.UtcNow.AddDays(-7);
                var allBlog = await _session
                    .Query<ContentItem, ContentItemIndex>(index => index.ContentType == "BlogPost")
                    .ListAsync();

                var blogsToSend = allBlog.Where(x => x.PublishedUtc > weekThreshold).ToList();

                string body = CreateNewsLetterBodyAsync(blogsToSend);

                foreach (var contentItem in subscribers)
                {
                    var subscriber = contentItem.As<SubscriberPart>();
                    body = body.Replace("{Name}", subscriber.Name);
                    string bodyFull = body.Replace("{Email}", subscriber.Email);
                    await SendEmail(subscriber.Email, bodyFull);
                }
            }
        }

        private string CreateNewsLetterBodyAsync(List<ContentItem> blogs)
        {
            string body = string.Empty;

            var assembly = Assembly.GetExecutingAssembly();
            string resourceName = assembly.GetManifestResourceNames().Single(str => str.EndsWith("emailTemplate.html"));

            Stream stream = assembly.GetManifestResourceStream(resourceName);
            StreamReader reader = new StreamReader(stream);
            body = reader.ReadToEnd();

            string links = String.Empty;
            foreach (ContentItem item in blogs)
            {
                var urlLink = "https://localhost:44300/" + item.Content.AutoroutePart.Path;
                links += "<a href='" + urlLink + "'>" + item.DisplayText + "</a>\n";
            }

            body = body.Replace("{Links}", links);
            return body;
        }

        private Task SendEmail(string address, string body)
        {
            string userName = "";
            string password = "";

            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(userName, password);
            client.UseDefaultCredentials = false;
            client.Credentials = credentials;

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(userName);
            msg.To.Add(new MailAddress(address));

            msg.Subject = "Your favorite blog's newsletter";
            msg.IsBodyHtml = true;
            msg.Body = body;

            try
            {
                client.Send(msg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>>>>> ERROR SENDING EMAIL! : {0}", ex.Message);
            }

            return Task.CompletedTask;
        }
    }
}

using System;
using AssignmentWebsite.Module.Drivers;
using AssignmentWebsite.Module.Handlers;
using AssignmentWebsite.Module.Indexes;
using AssignmentWebsite.Module.Migrations;
using AssignmentWebsite.Module.Models;
using AssignmentWebsite.Module.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using OrchardCore.BackgroundTasks;
using OrchardCore.ContentManagement;
using OrchardCore.ContentManagement.Display.ContentDisplay;
using OrchardCore.Data.Migration;
using OrchardCore.Modules;
using YesSql.Indexes;

namespace AssignmentWebsite.Module
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services
                .AddContentPart<SubscriberPart>()
                .UseDisplayDriver<SubscriberPartDisplayDriver>()
                .AddHandler<SubscriberPartHandler>();
            services.AddScoped<IDataMigration, NewsLetterMigrations>();
            services.AddSingleton<IIndexProvider, SubscriberPartIndexProvider>();

            services
                .AddContentPart<SubscribeWidgetPart>()
                .UseDisplayDriver<SubscribeWidgetPartDisplayDriver>();

            services.AddSingleton<IBackgroundTask, NewsLetterSchedulerTask>();
        }

        public override void Configure(IApplicationBuilder builder, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
                name: "Home",
                areaName: "AssignmentWebsite.Module",
                pattern: "Home/Index",
                defaults: new { controller = "Home", action = "Index" }
            );
        }
    }
}

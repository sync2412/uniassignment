﻿using AssignmentWebsite.Module.Indexes;
using AssignmentWebsite.Module.Models;
using OrchardCore.ContentManagement.Metadata;
using OrchardCore.ContentManagement.Metadata.Settings;
using OrchardCore.Data.Migration;
using YesSql.Sql;

namespace AssignmentWebsite.Module.Migrations
{
    public class NewsLetterMigrations : DataMigration
    {
        private readonly IContentDefinitionManager _contentDefinitionManager;

        public NewsLetterMigrations(IContentDefinitionManager contentDefinitionManager)
        {
            _contentDefinitionManager = contentDefinitionManager;
        }

        public int Create()
        {
            _contentDefinitionManager.AlterPartDefinition(nameof(SubscriberPart), part => part
                .Attachable()
            );

            _contentDefinitionManager.AlterTypeDefinition("SubscriberPage", type => type
                .Creatable()
                .Listable()
                .WithPart(nameof(SubscriberPart))
            );

            SchemaBuilder.CreateMapIndexTable<SubscriberPartIndex>(table => table
                .Column<string>(nameof(SubscriberPartIndex.ContentItemId), column => column.WithLength(26))
            );

            SchemaBuilder.AlterTable(nameof(SubscriberPartIndex), table => table
                .CreateIndex($"IDX_{nameof(SubscriberPartIndex)}_{nameof(SubscriberPartIndex.ContentItemId)}", nameof(SubscriberPartIndex.ContentItemId))
            );

            _contentDefinitionManager.AlterPartDefinition(nameof(SubscribeWidgetPart), part => part
                .Attachable()
            );

            _contentDefinitionManager.AlterTypeDefinition("SubscribeWidget", type => type
                .WithPart(nameof(SubscribeWidgetPart))
                .Stereotype("Widget")
            );

            return 5;
        }

        public int UpdateFrom1()
        {
            SchemaBuilder.CreateMapIndexTable<SubscriberPartIndex>(table => table
                .Column<string>(nameof(SubscriberPartIndex.ContentItemId), column => column.WithLength(26))
            );

            SchemaBuilder.AlterTable(nameof(SubscriberPartIndex), table => table
                .CreateIndex($"IDX_{nameof(SubscriberPartIndex)}_{nameof(SubscriberPartIndex.ContentItemId)}", nameof(SubscriberPartIndex.ContentItemId))
            );

            return 2;
        }

        public int UpdateFrom2()
        {
            _contentDefinitionManager.AlterTypeDefinition("SubscriberWidget", type => type
                .WithPart(nameof(SubscriberPart))
                .Stereotype("Widget")
            );

            return 3;
        }

        public int UpdateFrom3()
        {
            _contentDefinitionManager.AlterPartDefinition(nameof(SubscribeWidgetPart), part => part
                .Attachable()
            );

            _contentDefinitionManager.AlterTypeDefinition("SubscribeWidgetPage", type => type
                .Creatable()
                .Listable()
                .WithPart(nameof(SubscribeWidgetPart))
            );

            _contentDefinitionManager.AlterTypeDefinition("SubscribeWidget", type => type
                .WithPart(nameof(SubscribeWidgetPart))
                .Stereotype("Widget")
            );

            return 4;
        }
    }
}

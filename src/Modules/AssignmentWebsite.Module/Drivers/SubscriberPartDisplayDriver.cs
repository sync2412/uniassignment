﻿using OrchardCore.ContentManagement.Display.ContentDisplay;
using OrchardCore.ContentManagement.Display.Models;
using OrchardCore.DisplayManagement.ModelBinding;
using OrchardCore.DisplayManagement.Views;
using System.Threading.Tasks;
using AssignmentWebsite.Module.Models;
using AssignmentWebsite.Module.ViewModels;

namespace AssignmentWebsite.Module.Drivers
{
    public class SubscriberPartDisplayDriver : ContentPartDisplayDriver<SubscriberPart>
    {
        public override IDisplayResult Display(SubscriberPart part, BuildPartDisplayContext context) =>
            Initialize<SubscriberPartViewModel>(
                GetDisplayShapeType(context),
                viewModel => PopulateViewModel(part, viewModel))
            .Location("Detail", "Content:5")
            .Location("Summary", "Content:5");

        public override IDisplayResult Edit(SubscriberPart part, BuildPartEditorContext context) =>
            Initialize<SubscriberPartViewModel>(
                GetEditorShapeType(context),
                viewModel => PopulateViewModel(part, viewModel))
            .Location("Content:5");

        public override async Task<IDisplayResult> UpdateAsync(SubscriberPart part, IUpdateModel updater, UpdatePartEditorContext context)
        {
            var viewModel = new SubscriberPartViewModel();

            await updater.TryUpdateModelAsync(viewModel, Prefix);

            // Populate part from view model here.

            part.Name = viewModel.Name;
            part.Email = viewModel.Email;

            return await EditAsync(part, context);
        }

        private static void PopulateViewModel(SubscriberPart part, SubscriberPartViewModel viewModel)
        {
            // Populate view model from part here.

            viewModel.Name = part.Name;
            viewModel.Email = part.Email;
        }
    }
}

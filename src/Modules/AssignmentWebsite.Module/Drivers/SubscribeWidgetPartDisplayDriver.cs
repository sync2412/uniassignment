﻿using OrchardCore.ContentManagement.Display.ContentDisplay;
using OrchardCore.ContentManagement.Display.Models;
using OrchardCore.DisplayManagement.ModelBinding;
using OrchardCore.DisplayManagement.Views;
using System.Threading.Tasks;
using AssignmentWebsite.Module.Models;
using AssignmentWebsite.Module.ViewModels;

namespace AssignmentWebsite.Module.Drivers
{
    public class SubscribeWidgetPartDisplayDriver : ContentPartDisplayDriver<SubscribeWidgetPart>
    {
        public override IDisplayResult Display(SubscribeWidgetPart part, BuildPartDisplayContext context) =>
            Initialize<SubscribeWidgetPartViewModel>(
                GetDisplayShapeType(context),
                viewModel => PopulateViewModel(part, viewModel))
            .Location("Detail", "Content:5")
            .Location("Summary", "Content:5");

        public override IDisplayResult Edit(SubscribeWidgetPart part, BuildPartEditorContext context) =>
            Initialize<SubscribeWidgetPartViewModel>(
                GetEditorShapeType(context),
                viewModel => PopulateViewModel(part, viewModel))
            .Location("Content:5");

        public override async Task<IDisplayResult> UpdateAsync(SubscribeWidgetPart part, IUpdateModel updater, UpdatePartEditorContext context)
        {
            var viewModel = new SubscribeWidgetPartViewModel();

            await updater.TryUpdateModelAsync(viewModel, Prefix);

            // Populate part from view model here.

            return await EditAsync(part, context);
        }

        private static void PopulateViewModel(SubscribeWidgetPart part, SubscribeWidgetPartViewModel viewModel)
        {
            // Populate view model from part here.
        }
    }
}
